#!/bin/bash

# Tests some of the AWS Ansible authentication options for the aws_ec2_vpc role.
# The security token is not currently tested.

# Makes backups of the ~/.aws/credentials and ~/.boto configuration files and
# and restores them at the end.

# Run tests from the paultsparks-ansible-roles/aws_roles_testing project

# NOTE: Currently does not handle credentials in the ~/.aws config file. This file
# should be manually moved to a different filename if it contains credentials.

# The test credentials should be supplied in the environment variables AWS_ACCESS
# and AWS_SECRET. They will be prompted for if they are not set.

if [[ "$AWS_ACCESS" == "" ]]; then
    echo "AWS credentials have not been prepared."
    echo -n "Enter AWS access key: "
    read -s AWS_ACCESS
    export AWS_ACCESS
    echo
    
    echo -n "Enter AWS secret key: "
    read -s AWS_SECRET
    export AWS_SECRET
    echo
fi

if [[ -f  ~/.aws/credentials-backup ]] ; then
    echo "Found existing backup file  ~/.aws/credentials-backup  Move it and rerun."
    exit 1
fi
mv -n ~/.aws/credentials ~/.aws/credentials-backup

if [[ -f  ~/.boto ]] ; then
    echo "Found existing backup file  ~/.boto-backup  Move it and rerun."
    exit 1
fi
mv -n ~/.boto ~/.boto-backup


echo "aws_access=${AWS_ACCESS}"
echo "aws_secret=${AWS_SECRET:0:3}..."
unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY

# First verify that no creds can be found
ansible-playbook ext-roles/ec2_vpc/test/auth_fail.yml

# Check ENV variables
export AWS_ACCESS_KEY_ID="${AWS_ACCESS}"
export AWS_SECRET_ACCESS_KEY="${AWS_SECRET}"
echo
echo "Running auth_env test with AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID"
echo "AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:0:3}..."
ansible-playbook ext-roles/ec2_vpc/test/auth_env.yml

unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
echo "Unset AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID"
echo "AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:0:}..."

echo
echo "Running with boto profile and explicit values"
ansible-playbook ext-roles/ec2_vpc/test/auth_profile.yml

[[ -f ~/.aws/credentials-backup ]] && mv -f ~/.aws/credentials-backup ~/.aws/credentials
[[ -f ~/.boto-backup ]] && mv -f ~/.boto-backup ~/.boto

---

- hosts: localhost
  gather_facts: no
  vars:
    az: us-east-1d
    default_region: "us-east-1"
  tasks:
  - set_stats:
      data:
        tests: 0
        error: 0

  - block:
    - name: test VPC
      include_role:
        name: ec2_vpc
      vars:
        aws_ec2_vpc_values:
          igw: yes
          route_tables:
            - name: "Custom route table"
              subnets:
                - "Subnet 1"
              routes:
                - dest: 0.0.0.0/0
                  gateway_id: igw
          subnets:
            - name: "Subnet 1"
              cidr: "10.0.0.0/24"
              public: yes
              az: "{{ az }}"

    - debug: var=aws_vpc.route_tables
      
    - name: verify returned values
      assert:
        that:
          - "aws_vpc.vpc.instance_tenancy == 'default'"
          - "aws_vpc.vpc.tags.Name == 'myapp-vpc'"
          - "aws_vpc.vpc.is_default == False"
          - "aws_vpc.vpc.state == 'available'"
          - "aws_vpc.vpc.cidr_block == '10.0.0.0/16'"
          - "aws_vpc.subnets | length == 1"
          - "aws_vpc.subnets[0].availability_zone == az"
          - "aws_vpc.subnets[0].state == 'available'"
          - "aws_vpc.subnets[0].default_for_az == False"
          - "aws_vpc.subnets[0].cidr_block == '10.0.0.0/24'"
          - "aws_vpc.subnets[0].tags.Name == 'Subnet 1'"
          - "aws_vpc.subnets[0].map_public_ip_on_launch == False"
          - "aws_vpc.subnets[0].vpc_id == aws_vpc.vpc.id"
          - "aws_vpc.nat_gateways |length == 0"
          - "aws_vpc.route_tables[0].vpc_id == aws_vpc.vpc.id"
          - "aws_vpc.route_tables | length == 1"
    - set_fact: rt="{{ aws_vpc.route_tables[0].routes | sort(attribute='destination_cidr_block') }}"
    - assert:
        that:
          - "rt[0].gateway_id == aws_vpc.internet_gateway_id"
          - "rt[0].nat_gateway_id is not defined"
          - "rt[0].instance_id is not defined"
          - "rt[0].destination_cidr_block == '0.0.0.0/0'"
          - "rt[0].state == 'active'"
          - "rt[1].gateway_id == 'local'"
          - "rt[1].nat_gateway_id is not defined"
          - "rt[1].instance_id is not defined"
          - "rt[1].destination_cidr_block == '10.0.0.0/16'"
          - "rt[1].state == 'active'"

    - ec2_vpc_net_facts:
        region: "{{ default_region }}"
        filters:
          "tag:Name": "myapp-vpc"
      register: t
    - set_fact: vpcs="{{ t.vpcs }}"
    - debug: msg="VPCs {{vpcs}}"
    - assert:
        that:
          - "vpcs | length == 1"
          - "vpcs[0].instance_tenancy == 'default' "
          - "vpcs[0].tags == {'Name':'myapp-vpc'}"
          - "vpcs[0].cidr_block == '10.0.0.0/16'"
          - "vpcs[0].enable_dns_support == True"
          - "vpcs[0].enable_dns_hostnames == True"
          - "vpcs[0].is_default == False"
          - "vpcs[0].state == 'available'"
          - "vpcs[0].tags.Name == 'myapp-vpc'"

    - ec2_vpc_subnet_facts:
        region: "{{ default_region }}"
        filters:
          vpc-id: "{{vpcs[0].vpc_id}}"
      register: t
    - set_fact: subnets="{{t.subnets}}"
    - debug: msg="Subnets {{subnets}}"
    - assert:
        that:
          - "subnets | length == 1"
          - "subnets[0].state == 'available'"
          - "subnets[0].assign_ipv6_address_on_creation == False"
          - "subnets[0].default_for_az == False"
          - "subnets[0].availability_zone == az"
          - "subnets[0].vpc_id == vpcs[0].vpc_id"
          - "subnets[0].cidr_block == '10.0.0.0/24'"
          - "subnets[0].available_ip_address_count == 251"
          - "subnets[0].map_public_ip_on_launch == False"
          - "subnets[0].tags.Name == 'Subnet 1'"

    - ec2_vpc_igw_facts:
        region: "{{ default_region }}"
        filters:
          attachment.vpc-id: "{{vpcs[0].vpc_id}}"
      register: t
    - set_fact: igws="{{t.internet_gateways}}"
    - debug: msg="IGWs {{igws}}"
    - assert:
        that:
          - "igws | length == 1"
          - "igws[0].attachments[0].vpc_id == vpcs[0].vpc_id"
          - "igws[0].attachments[0].state == 'available'"
          - "igws[0].tags[0].key == 'Name'"
          - "igws[0].tags[0].value == 'myapp-vpc-igw'"

    - ec2_vpc_route_table_facts:
        region: "{{ default_region }}"
        filters:
          vpc-id: "{{vpcs[0].vpc_id}}"
      register: t
    - set_fact: rt="{{ item }}"
      when: "item.tags.Name is defined and item.tags.Name == 'Custom route table'"
      loop: "{{ t.route_tables }}"
    - set_fact: rt_routes="{{rt.routes | sort(attribute='destination_cidr_block') }}"
    - assert:
        that:
          - "rt.tags.Name == 'Custom route table'"
          - "rt.associations[0].subnet_id == subnets[0].subnet_id"
          - "rt.associations[0].main == False"
          - "rt.vpc_id == vpcs[0].vpc_id"
          - "rt.routes | length == 2"
          - "rt_routes[0].gateway_id == igws[0].internet_gateway_id"
          - "rt_routes[0].state == 'active'"
          - "rt_routes[0].instance_id == None"
          - "rt_routes[0].vpc_peering_connection_id == None"
          - "rt_routes[0].destination_cidr_block == '0.0.0.0/0'"
          - "rt_routes[1].gateway_id == 'local'"
          - "rt_routes[1].state == 'active'"
          - "rt_routes[1].instance_id == None"
          - "rt_routes[1].vpc_peering_connection_id == None"
          - "rt_routes[1].destination_cidr_block == '10.0.0.0/16'"

    rescue:
      - debug: msg="rescue block"
      - set_stats:
          data:
            error: 1
  - set_stats:
      data:
        tests: 1

  - name: Get route tables
    ec2_vpc_route_table_facts:
      region: "{{ arg.region | default(omit) }}"
      filters:
        vpc-id: "{{vpc.id}}"
    register: tmp
  - debug: msg="Route tables= {{tmp}}"

  - block:
    - name: test VPC removal
      include_role:
        name: ec2_vpc
      vars:
        aws_ec2_vpc_values:
          state: absent

    - ec2_vpc_net_facts:
        region: "{{ default_region }}"
        filters:
          "tag:Name": "myapp-vpc"
      register: t
    - set_fact: vpcs="{{ t.vpcs }}"
    - debug: msg="VPCs {{vpcs}}"
    - assert:
        that:
          - "vpcs | length == 0"
    rescue:
      - debug: msg="rescue block"
      - set_stats:
          data:
            error: 1
  - set_stats:
      data:
        tests: 1
      

aws_ec2_vpc
=======
Ansible role to provision or decommission an AWS VPC.

Requirements
------------
boto >= 2.24.0  
Tested with:  
 ansible (2.5.3)  
 boto (2.44.0)  
 boto3 (1.9.86)  
 botocore (1.12.86)  

Role Variables
--------------
Takes a single hash / dictionary named **aws_ec2_vpc_values**. This helps 
prevent variable bleed with multiple invocations. The following keys may be 
included. All values are optional. Defaults are noted where appropriate.

**cidr_block** - IP address range for your VPC subnet. Default is **10.0.0.0/16**

**dns_assign_names** - Assign DNS names to instances Default is **yes**.

**dns_enable** - Enable DNS resolution for the VPC. Default is **yes**.

**igw** - If **yes**, create an Internet Gateway. Default is **no**.

**name** - VPC name. Will also be used as a prefix for related AWS names.
Default is **myapp-vpc**.

**region** - Target AWS region. Default: will use environment variable `AWS_REGION`
or `EC2_REGION` if defined or **us-east-1**.

**state** - Set to **absent** for decommissioning. Decommisioning requires
only region and **name** to be set. Default is **present**.

**subnets** - List of subnets to create. Default is 1 public subnet called "subnet1"
with 10.0.0.0/24 CIDR. Each list item is a dictionary with the following keys:
 * **name** - *Required* Name for your subnet
 * **cidr** - *Required* CIDR block for the subnet. Must be a portion of the VPC CIDR
 * **map_public** - If **yes**, auto assign public IPs to instances. Default is **no**.
 * **public** - If **yes**, the subnet will have Internet access through the igw. Default is **no**.
 * **tags** - Dictionary of user tags. Unlisted tags will be removed.
 * **az** - Availability zone. Default is AWS auto assignment.
 * **wait** - If **yes**, wait for the subnet to become available. Default is **yes**.
 * **wait_timeout** - Seconds to wait for subnet to be available. Default is **300**.

**tags** - Tags to assign to the VPC. Note **Name** will override the **name** variable.
On update, tags not listed will be removed.

**tenancy** - **dedicated** or **default** instances. Default is **default**.

#### Authorization values

**aws_access_key** - AWS access key value. If not set then the value of the
`AWS_ACCESS_KEY_ID`, `AWS_ACCESS_KEY` or `EC2_ACCESS_KEY` environment variable is used.

**aws_secret_key** - AWS secret key value. If not set then the value of the 
`AWS_SECRET_ACCESS_KEY`, `AWS_SECRET_KEY`, or `EC2_SECRET_KEY` environment variable is used.

**boto_profile** - A boto profile name.

**security_token** - AWS STS security token.  If not set then the value of the
`AWS_SECURITY_TOKEN` or `EC2_SECURITY_TOKEN` environment variable is used.



Return Values
-------------
The complex variable **aws_vpc** is returned for referencing VPC components:
 * **gateway_id** - *ec2_vpc_igw* result.gateway_id
 * **nat_gateways** - List of *ec2_vpc_nat_gateway* results
 * **route_tables** - List of *ec2_vpc_route_table* result.route_table
 * **subnets** - List of *ec2_vpc_subnet* result.subnet
 * **vpc** - *ec2_vpc* result.vpc
 


Dependencies
------------
None

Example Playbook
----------------
 Provisioning. 
 [AWS VPC scenario 1](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Scenario1.html)
```
- include_role:
    name: ec2_vpc
  vars:
    values:
      igw: yes
      route_tables:
        - name: "Custom route table"
          subnets:
            - "Subnet 1"
          routes:
            - dest: 0.0.0.0/0
              gateway_id: igw
      subnets:
        - name: "Subnet 1"
          cidr: "10.0.0.0/24"
          public: yes
          az: "{{ az }}"
```

 [AWS VPC scenario 2 ](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Scenario2.html)
```
- include_role:
    name: ec2_vpc
  vars:
    values:
      name: "aws_vpc2"
      igw: yes
      subnets:
        - name: "Public subnet"
          public: yes
          az: "{{ az }}"
          nat_gateway: yes
          nat_wait: yes
        - name: "Private subnet"
          cidr: "10.0.1.0/24"
          az: "{{ az }}"
      route_tables:
        - name: "Custom route table"
          subnets:
            - "Public subnet"
          routes:
            - dest: 0.0.0.0/0
              gateway_id: igw
        - name: main
          subnets:
            - "Private subnet"
          routes:
            - dest: 0.0.0.0/0
              nat_subnet: "Public subnet"
```

 Decommisioning.
```
- include_role:
    name: ec2_vpc
  vars:
    values:
      name: "aws_vpc2"
      state: absent
```
